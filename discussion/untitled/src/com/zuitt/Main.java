// directory but contains files to build application
package com.zuitt;

import java.util.Locale;

public class Main {
    public static void main(String[] args){
//        System.out.println("Hello World");
        int myNum;
        int myNum2 = 30;
        System.out.println("Result of variable declaration and initialization");
        System.out.println(myNum2);

        //constant
        final int PRINCIPAL=1000;

        //primitive - store simple values
        //eg. char, int, double, boolean


        //non-primitive - store complex values
        //eg. array , string

        System.out.println(((Object)PRINCIPAL).getClass());

        String name = "John Doe";

        System.out.println("Result of non-primitive data type");
        System.out.println(name);

        String editName= name.toLowerCase(Locale.ROOT);
        System.out.println(editName);


        //Type Casting

        //Implicit casting

        int num1=5;
        double num2 = 2.7;
        double total = num1+num2;
        System.out.println(total);

        //Explicit Casting
        int num3 = 5;
        double num4 = 2.7;
        int anotherTotal = num3+ (int)num4;
        System.out.println(anotherTotal);
    }
}
